use super::{Token, Token::*};

macro_rules! is_err {
    ($inp:expr) => {
        let t = super::parse($inp).next().unwrap();
        t.token.unwrap_err();
    };
}

macro_rules! parse {
    ($expr:expr) => {
        super::parse($expr)
            .map(|t| t.token)
            .collect::<Result<Vec<_>, ()>>()
            .expect("failed to parse")
    };
}

#[test]
fn raw() {
    assert_eq!(parse!("r#true"), [RawIdent("r#true")]);
}

#[test]
fn numbers() {
    let s = r#"let äöü = 29;"#;
    let out = parse!(s);
    assert_eq!(
        out,
        [
            Token::KwLet,
            Token::Text("äöü"),
            Token::Eq,
            Token::IntegerLiteral("29"),
            Token::Semi
        ]
    );

    assert_eq!(parse!("0b10"), [IntegerLiteral("0b10")]);
    assert_eq!(parse!("0b1"), [IntegerLiteral("0b1")]);
    assert_eq!(parse!("0b1000_1010"), [IntegerLiteral("0b1000_1010")]);

    assert_eq!(parse!("0o01_253"), [IntegerLiteral("0o01_253")]);

    assert_eq!(parse!("0o01_953"), [Text("0o01_953")]);

    assert_eq!(parse!("0x01_AF"), [IntegerLiteral("0x01_AF")]);

    assert_eq!(parse!("0x01_AFu16"), [IntegerLiteral("0x01_AFu16")]);
    assert_eq!(parse!("0x01_AFu17"), [Text("0x01_AFu17")]);
    assert_eq!(parse!("0x01_AFusize"), [IntegerLiteral("0x01_AFusize")]);

    assert_eq!(parse!("Feed"), [Text("Feed")]);

    assert_eq!(parse!("__"), [Text("__")]);
    assert_eq!(parse!("0b__"), [Text("0b__")]);
    assert_eq!(parse!("0b_1"), [IntegerLiteral("0b_1")]);

    assert_eq!(parse!("10."), [FloatLiteral("10.")]);

    assert_eq!(parse!("123.0f64;"), [FloatLiteral("123.0f64"), Semi]);
    assert_eq!(parse!("0.1f64;"), [FloatLiteral("0.1f64"), Semi]);
    assert_eq!(parse!("0.1f32;"), [FloatLiteral("0.1f32"), Semi]);
    assert_eq!(parse!("5f32;"), [FloatLiteral("5f32"), Semi]);
    assert_eq!(parse!("5E3f32;"), [FloatLiteral("5E3f32"), Semi]);
    assert_eq!(parse!("5.3E3f32;"), [FloatLiteral("5.3E3f32"), Semi]);

    is_err!("12.3E_f64;");
    assert_eq!(parse!("12.3E_3;"), [FloatLiteral("12.3E_3"), Semi]);

    assert_eq!(
        parse!("12.3E-99_f64;"),
        [FloatLiteral("12.3E-99_f64"), Semi]
    );
    assert_eq!(parse!("12E+99_f64;"), [FloatLiteral("12E+99_f64"), Semi]);
}

#[test]
fn invalid_chars_and_labels() {
    println!("char literals:");
    is_err!("'\t'");
    is_err!("'\n'");
    is_err!("'\r'");
    is_err!(r"'	'");

    println!("labels");
    is_err!(r"' ");
    is_err!(" '\t");
    is_err!(r"'\t");
    is_err!(r"'	");
    is_err!(" '\n");
    is_err!(r"'\n");
    is_err!(" '\r");
}

#[test]
fn chars_lifetimes_and_labels() {
    assert_eq!(parse!("'a'"), [CharLiteral('a')]);
    assert_eq!(parse!("'ä'"), [CharLiteral('ä')]);
    assert_eq!(parse!("'🦁'"), [CharLiteral('🦁')]);
    assert_eq!(parse!(r"' '"), [CharLiteral(' ')]);
    assert_eq!(parse!(r"'\\'"), [CharLiteral('\\')]);
    assert_eq!(parse!(r"'\t'"), [CharLiteral('\t')]);
    assert_eq!(parse!(r"'\n'"), [CharLiteral('\n')]);
    // TODO:
    // assert_eq!(parse!(r"'\x20'"), [Char('\x20')]);
    // assert_eq!(parse!(r"'\u{2211}'"), [Char('\u{2211}')]);
    assert_eq!(parse!(r#"'"'"#), [CharLiteral('"')]);
    assert_eq!(parse!(r"'\''"), [CharLiteral('\'')]);

    assert_eq!(parse!(r"'_'"), [CharLiteral('_')]);
    assert_eq!(parse!(r"'_"), [Label("_")]);

    assert_eq!(
        parse!(r"'test, 'more"),
        [Label("test"), Comma, Label("more")]
    );

    assert_eq!(parse!("'a"), [Label("a")]);
    assert_eq!(parse!("'static"), [Label("static")]);
    assert_eq!(parse!("'main"), [Label("main")]);
}

#[test]
fn parse_unicode() {
    let s = r#"let äöü = "bäbä test 🦁 blabla";"#;
    let out = parse!(s);
    assert_eq!(
        out,
        [
            Token::KwLet,
            Token::Text("äöü"),
            Token::Eq,
            Token::StringLiteral("bäbä test 🦁 blabla"),
            Token::Semi
        ]
    )
}

#[test]
fn perfaced_escaped_str() {
    let s = r##"let äöü = r#"bäbä test " blabla"#;"##;
    let out = parse!(s);
    assert_eq!(
        out,
        [
            Token::KwLet,
            Token::Text("äöü"),
            Token::Eq,
            Token::RawStringLiteral("bäbä test \" blabla"),
            Token::Semi
        ]
    );

    let s = r###"let äöü = r##"bäbä test " "# blabla"##;"###;
    let out = parse!(s);
    assert_eq!(
        out,
        [
            Token::KwLet,
            Token::Text("äöü"),
            Token::Eq,
            Token::RawStringLiteral("bäbä test \" \"# blabla"),
            Token::Semi
        ]
    );

    let s = r######"let äöü = r##"bäbä test " "# blabla"##;  r#####""####"#####;"######;

    let out = parse!(s);
    assert_eq!(
        out,
        [
            Token::KwLet,
            Token::Text("äöü"),
            Token::Eq,
            Token::RawStringLiteral("bäbä test \" \"# blabla"),
            Token::Semi,
            Token::RawStringLiteral("\"####"),
            Token::Semi,
        ]
    )
}

#[test]
fn perfaced_str() {
    let s = r#"let äöü = r"bäbä \test \n blabla";"#;
    let out = parse!(s);
    assert_eq!(
        out,
        [
            Token::KwLet,
            Token::Text("äöü"),
            Token::Eq,
            Token::RawStringLiteral(r"bäbä \test \n blabla"),
            Token::Semi
        ]
    )
}

#[test]
fn byte() {
    let s = r#"let äöü = b"bäbä \test \n blabla";"#;
    let out = parse!(s);
    assert_eq!(
        out,
        [
            Token::KwLet,
            Token::Text("äöü"),
            Token::Eq,
            Token::ByteLiteral(r"bäbä \test \n blabla"),
            Token::Semi
        ]
    )
}
