use logos::{Lexer, Logos, SpannedIter};

pub use logos::Span;

mod char;
use crate::char::{label_char, LabelChar};

fn string_literal<'a>(l: &mut Lexer<'a, Token<'a>>) -> Option<&'a str> {
    let mut esc = false;
    let mut i = 0;

    for ch in l.remainder().chars() {
        i += ch.len_utf8();

        if esc {
            esc = false
        } else if ch == '\\' {
            esc = true
        } else if ch == '"' {
            let end = i;
            let out = l.remainder().split_at(end - 1).0;
            l.bump(end);
            return Some(out);
        }
    }

    None
}

fn raw_string_literal<'a>(l: &mut Lexer<'a, Token<'a>>) -> Option<&'a str> {
    let mut esc = None;
    let mut i = 0;

    // TODO: track newlines

    let escapes: u8 = l
        .slice()
        .chars()
        .filter(|&ch| ch == '#')
        .count()
        .try_into()
        .ok()?;

    for ch in l.remainder().chars() {
        i += ch.len_utf8();

        if ch == '"' {
            if escapes == 0 {
                let out = &l.remainder()[..(i - 1)];
                l.bump(i);
                return Some(out);
            }
            esc = Some(0u8);
        } else if let Some(n) = esc.as_mut() {
            if ch == '#' {
                *n += 1;
                if *n == escapes {
                    let out = &l.remainder()[..((i - 1) - escapes as usize)];
                    l.bump(i);
                    return Some(out);
                }
            } else {
                esc = None;
            }
        }
    }

    None
}

fn block_comment<'a>(l: &mut Lexer<'a, Token<'a>>) -> Option<&'a str> {
    let mut level = 1;
    let mut chars = l.remainder().chars();
    let mut i = 0;

    // TODO: track newlines
    while let Some(ch) = chars.next() {
        i += ch.len_utf8();

        if ch == '*' && chars.next()? == '/' {
            level -= 1;
            i += 1;
        } else if ch == '/' && chars.next()? == '*' {
            level += 1;
            i += 1;
        }

        if level == 0 {
            l.bump(i);
            return Some(l.slice());
        }
    }

    None
}

#[derive(Logos, Debug, PartialEq, Eq)]
#[logos(skip r"[ \t\f]+")]
pub enum Token<'a> {
    // Keywords {
    #[token("as")]
    KwAs,
    #[token("break")]
    KwBreak,
    #[token("const")]
    KwConst,
    #[token("continue")]
    KwContinue,
    #[token("crate")]
    KwCrate,
    #[token("else")]
    KwElse,
    #[token("enum")]
    KwEnum,
    #[token("extern")]
    KwExtern,
    #[token("false")]
    KwFalse,
    #[token("fn")]
    KwFn,
    #[token("for")]
    KwFor,
    #[token("if")]
    KwIf,
    #[token("impl")]
    KwImpl,
    #[token("in")]
    KwIn,
    #[token("let")]
    KwLet,
    #[token("loop")]
    KwLoop,
    #[token("match")]
    KwMatch,
    #[token("mod")]
    KwMod,
    #[token("move")]
    KwMove,
    #[token("mut")]
    KwMut,
    #[token("pub")]
    KwPub,
    #[token("ref")]
    KwRef,
    #[token("return")]
    KwReturn,
    #[token("self")]
    KwSelfvalue,
    #[token("Self")]
    KwSelftype,
    #[token("static")]
    KwStatic,
    #[token("struct")]
    KwStruct,
    #[token("super")]
    KwSuper,
    #[token("trait")]
    KwTrait,
    #[token("true")]
    KwTrue,
    #[token("type")]
    KwType,
    #[token("unsafe")]
    KwUnsafe,
    #[token("use")]
    KwUse,
    #[token("where")]
    KwWhere,
    #[token("while")]
    KwWhile,

    #[token("async")]
    KwAsync,
    #[token("await")]
    KwAwait,
    #[token("dyn")]
    KwDyn,

    #[token("abstract")]
    KwAbstract,
    #[token("become")]
    KwBecome,
    #[token("box")]
    KwBox,
    #[token("do")]
    KwDo,
    #[token("final")]
    KwFinal,
    #[token("macro")]
    KwMacro,
    #[token("override")]
    KwOverride,
    #[token("priv")]
    KwPriv,
    #[token("typeof")]
    KwTypeof,
    #[token("unsized")]
    KwUnsized,
    #[token("virtual")]
    KwVirtual,
    #[token("yield")]
    KwYield,

    #[token("try")]
    KwTry,
    // End of keywords }
    #[regex("r#\\w+")]
    RawIdent(&'a str),

    #[regex(r"//.*")]
    LineComment(&'a str),
    #[regex("///.*")]
    DocComment(&'a str),

    #[token("/*", block_comment)]
    BlockComment(&'a str),

    #[regex(r"[\w_]+")]
    Text(&'a str),

    #[token("'", label_char)]
    LabelOrChar(LabelChar<'a>),

    Label(&'a str),
    CharLiteral(char),

    #[token("\"", string_literal)]
    StringLiteral(&'a str),
    #[regex("r#*\"", raw_string_literal)]
    RawStringLiteral(&'a str),
    #[token("b\"", string_literal)]
    ByteLiteral(&'a str),
    #[token("br#*\"", raw_string_literal)]
    RawByteLiteral(&'a str),

    #[regex(
        "(\
0b[01_]*[01][01_]*\
|0o[0-6_]*[0-6][0-6_]*\
|0x[0-9a-fA-F_]*[0-9a-fA-F][0-9a-fA-F_]*\
|[0-9][0-9_]*\
)([ui](8|16|32|64|128|size))?",
        priority = 3
    )]
    IntegerLiteral(&'a str),

    #[regex("[0-9]+(\\.[0-9]*)?([eE][+-]?_*[0-9][0-9_]*)?(f32|f64)?", priority = 2)]
    FloatLiteral(&'a str),

    #[token("=>")]
    DoubleArrow,
    #[token("->")]
    SingleArrow,

    #[token("::")]
    DoubleColon,

    #[token(":")]
    Colon,
    #[token(";")]
    Semi,
    #[token(",")]
    Comma,
    #[token(".")]
    Dot,

    #[token("..")]
    DotDot,
    #[token("..=")]
    DotDotEq,

    #[token("...")]
    DotDotDot,

    #[token("(")]
    OpenParen,
    #[token(")")]
    CloseParen,
    #[token("{")]
    OpenBrace,
    #[token("}")]
    CloseBrace,
    #[token("[")]
    OpenBracket,
    #[token("]")]
    CloseBracket,

    #[token("@")]
    At,
    #[token("#")]
    Hash,
    #[token("~")]
    Tilde,
    #[token("?")]
    Question,
    #[token("$")]
    Dollar,
    #[token("=")]
    Eq,
    #[token("!")]
    Bang,

    #[token("&&")]
    AndAnd,
    #[token("||")]
    OrOr,
    #[token("<<")]
    Shl,
    #[token(">>")]
    Shr,
    #[token("+=")]
    PlusEq,
    #[token("-=")]
    MinusEq,
    #[token("*=")]
    StarEq,
    #[token("/=")]
    SlashEq,
    #[token("%=")]
    PercentEq,
    #[token("^=")]
    CaretEq,
    #[token("&=")]
    AndEq,
    #[token("|=")]
    OrEq,

    #[token("<<=")]
    ShlEq,
    #[token(">>=")]
    ShrEq,

    #[token("==")]
    EqEq,
    #[token("!=")]
    NotEq,
    #[token(">=")]
    GtEq,
    #[token("<=")]
    LtEq,

    #[token("<")]
    Lt,
    #[token(">")]
    Gt,
    #[token("-")]
    Minus,
    #[token("&")]
    Amp,
    #[token("|")]
    Or,
    #[token("+")]
    Plus,
    #[token("*")]
    Star,
    #[token("/")]
    Slash,
    #[token("^")]
    Caret,
    #[token("%")]
    Percent,

    #[token("\n")]
    NewLine,
}

impl<'a> Token<'a> {
    fn seperate(self) -> Self {
        match self {
            Token::LabelOrChar(t) => t.into(),
            t => t,
        }
    }
}

pub struct TokenSpan<'a> {
    pub token: Result<Token<'a>, ()>,
    pub span: Span,
}

type TokRes<'a> = (Result<Token<'a>, ()>, Span);

impl<'a> From<TokRes<'a>> for TokenSpan<'a> {
    fn from((token, span): (Result<Token<'a>, ()>, Span)) -> Self {
        Self {
            token: token.map(Token::seperate),
            span,
        }
    }
}

pub struct Tokens<'a> {
    inner: SpannedIter<'a, Token<'a>>,
}

impl<'a> Iterator for Tokens<'a> {
    type Item = TokenSpan<'a>;

    fn next(&mut self) -> Option<TokenSpan<'a>> {
        self.inner.next().map(Into::into)
    }
}

pub fn parse(s: &str) -> Tokens {
    let inner = Token::lexer(s).spanned();
    Tokens { inner }
}

#[cfg(test)]
mod tests;
