use super::{Lexer, Token};

#[derive(Debug, PartialEq, Eq)]
pub enum LabelChar<'a> {
    Label(&'a str),
    Char(char),
}

impl<'a> From<LabelChar<'a>> for Token<'a> {
    fn from(value: LabelChar<'a>) -> Self {
        match value {
            LabelChar::Label(s) => Token::Label(s),
            LabelChar::Char(c) => Token::CharLiteral(c),
        }
    }
}

fn esc_chars<I>(mut chars: std::iter::Peekable<I>) -> Option<(char, usize)>
where
    I: Iterator<Item = char>,
{
    let ch = chars.next();
    if chars.peek() == Some(&'\'') {
        let ch = match ch {
            Some('\\') => Some('\\'),
            Some('\"') => Some('\"'),
            Some('\'') => Some('\''),
            Some('n') => Some('\n'),
            Some('t') => Some('\t'),
            Some('r') => Some('\r'),
            Some('0') => Some('\0'),
            Some(c) => todo!("handle {c:?}"),
            None => None,
        };
        return ch.map(|i| (i, 3));
    }

    match ch {
        Some('x') => {
            todo!();
        }
        Some('u') => {
            // TODO: bump
            chars.next().filter(|&ch| ch == '{')?;

            let mut codepoints = [0; 4];
            let mut i = 0;
            while chars
                .peek()
                .filter(|&&ch| ch.is_digit(16) || ch == '_')
                .is_some()
            {
                let ch = chars.next().unwrap();
                if ch == '_' {
                    continue;
                }
                codepoints[i] = ch.to_digit(16).unwrap() as u8;
                i += 1;
            }

            let ch = char::from_u32(u32::from_be_bytes(codepoints));

            println!("{ch:?} {codepoints:?}");

            chars.next().filter(|&ch| ch == '}')?;
            chars.next().filter(|&ch| ch == '\'')?;

            ch.map(|c| (c, i))
        }
        Some(_) => None,
        None => None,
    }
}

pub fn label_char<'a>(l: &mut Lexer<'a, Token<'a>>) -> Option<LabelChar<'a>> {
    let mut chars = l.remainder().chars().peekable();

    match chars.next() {
        Some('\\') => {
            let (ch, bump) = esc_chars(chars)?;
            l.bump(bump);
            Some(LabelChar::Char(ch))
        }
        Some(c) if chars.next() == Some('\'') => {
            l.bump(c.len_utf8() + 1);
            if ['\t', '\n', '\r'].contains(&c) {
                None
            } else {
                Some(LabelChar::Char(c))
            }
        }
        Some('\t' | '\n' | '\r' | ' ') => {
            println!("inv label");
            l.bump(1);
            None
        }
        Some(_) => {
            let mut rem = l.remainder();

            if let Some(i) = rem
                .chars()
                .position(|ch| !(ch.is_alphabetic() || ch == '_'))
            {
                rem = &rem[..i]
            }

            l.bump(rem.len());

            Some(LabelChar::Label(rem))
        }
        None => None,
    }
}
