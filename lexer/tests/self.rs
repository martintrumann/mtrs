use lexer::parse;

use std::fs::read_to_string;

/// and a doc string
#[test]
fn this() {
    let s = read_to_string("./tests/self.rs").unwrap();
    let mut out = parse(&s);
    assert!(out.all(|t| t.token.is_ok()));
}

#[test]
fn lib() {
    let s = read_to_string("./src/lib.rs").unwrap();
    let mut out = parse(&s);
    assert!(out.all(|t| t.token.is_ok()));
}

#[test]
fn char() {
    let s = read_to_string("./src/char.rs").unwrap();
    let mut out = parse(&s);
    assert!(out.all(|t| t.token.is_ok()));
}
