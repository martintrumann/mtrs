use crate::{attribute::Attribute, item::Item};

pub mod block;

#[derive(Debug)]
pub enum Statment<'a> {
    Item(Item<'a>),
    LetStatment(Let<'a>),
    Expression(Expression<'a>),
    MacroInvocation(),
}

#[derive(Debug)]
pub struct Let<'a> {
    attributes: Vec<Attribute<'a>>,
    ident: &'a str,
    expr: Expression<'a>,
    els: Option<Expression<'a>>,
}

#[derive(Debug)]
pub enum Expression<'a> {
    BlockExpression(block::BlockExpression<'a>),
}
