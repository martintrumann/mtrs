#[derive(Debug)]
pub struct Type<'a>(pub &'a str);
