use lexer::{Span, Token, TokenSpan};

pub struct Tokens<'a> {
    inner: lexer::Tokens<'a>,
    peek: Option<TokenSpan<'a>>,
    col: u64,
    col_start: usize,
}

impl<'a> Tokens<'a> {
    pub fn new(mut inner: lexer::Tokens<'a>) -> Self {
        let peek = inner.next().map(Into::into);
        Self {
            inner,
            peek,
            col: 0,
            col_start: 0,
        }
    }
}

pub struct Loc {
    pub col: u64,
    pub row: Span,
}

impl Loc {
    fn calc(col: u64, col_start: usize, span: &Span) -> Self {
        Self {
            col,
            row: Span {
                start: span.start - col_start,
                end: span.end - col_start,
            },
        }
    }
}

impl Tokens<'_> {
    pub fn col(&self) -> (u64, usize) {
        (self.col, self.col_start)
    }

    pub fn loc(&self) -> (u64, Option<Span>) {
        (
            self.col,
            self.peek.as_ref().map(|p| Span {
                start: p.span.start - self.col_start,
                end: p.span.end - self.col_start,
            }),
        )
    }

    pub fn span(&self) -> Option<&Span> {
        Some(&self.peek.as_ref()?.span)
    }

    pub fn peek(&self) -> Option<&Token> {
        Some(self.peek.as_ref()?.token.as_ref().ok()?)
    }

    pub fn next_is(&self, t: Token) -> bool {
        self.peek() == Some(&t)
    }
}

impl<'a> Iterator for Tokens<'a> {
    type Item = (TokenSpan<'a>, Loc);

    fn next(&mut self) -> Option<(TokenSpan<'a>, Loc)> {
        let cur = self.peek.take()?;
        self.peek = self.inner.next().map(Into::into);

        if matches!(cur.token, Ok(Token::NewLine)) {
            self.col += 1;
            self.col_start = cur.span.end;
            self.next()
        } else {
            let loc = Loc::calc(self.col, self.col_start, &cur.span);
            Some((cur, loc))
        }
    }
}

#[test]
fn peekable_tokens() {
    let mut t = Tokens::new(lexer::parse("= , fn [ ]"));
    assert!(t.next_is(Token::Eq));
    assert_eq!(t.next().unwrap().0.token.unwrap(), Token::Eq);
    assert_eq!(t.next().unwrap().0.token.unwrap(), Token::Comma);

    let mut t = Tokens::new(lexer::parse("= , fn [ ]"));
    assert_eq!(t.next().unwrap().0.token.unwrap(), Token::Eq);
    assert_eq!(t.peek().unwrap(), &Token::Comma);
    assert_eq!(t.next().unwrap().0.token.unwrap(), Token::Comma);
    assert_eq!(t.next().unwrap().0.token.unwrap(), Token::KwFn);
    assert_eq!(t.next().unwrap().0.token.unwrap(), Token::OpenBracket);
    assert_eq!(t.next().unwrap().0.token.unwrap(), Token::CloseBracket);
}
