use lexer::Token;

// use crate::path::SimplePath;
use crate::Parse;

#[derive(Debug, Default, PartialEq)]
pub enum Visibility {
    #[default]
    None,
    All,
    Crate,
    Super,
    // In(SimplePath),
}

impl Parse for Visibility {
    fn check(toks: &crate::tokens::Tokens) -> bool {
        toks.next_is(lexer::Token::KwPub)
    }

    fn parse(toks: &mut crate::tokens::Tokens, e: &mut crate::Errors) -> Option<Self> {
        if toks.next()?.0.token != Ok(lexer::Token::KwPub) {
            return e.push("Pub with nothing", toks);
        }

        if toks.next_is(lexer::Token::OpenParen) {
            toks.next();
            let t = toks.next()?;
            if !toks.next_is(Token::CloseParen) {
                todo!("error when no close paren")
            };
            toks.next()?;
            match t.0.token.ok()? {
                Token::KwCrate => Some(Self::Crate),
                Token::KwSuper => Some(Self::Super),
                _ => None,
            }
        } else {
            Some(Self::All)
        }
    }
}

#[test]
fn parse() {
    parse!("pub", Visibility::All);
    parse!("pub(crate)", Visibility::Crate);
    parse!("pub(super)", Visibility::Super);
}
