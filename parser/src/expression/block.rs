use crate::attribute::Attribute;

#[derive(Debug)]
pub struct BlockExpression<'a> {
    attributes: Vec<Attribute<'a>>,
    statments: Vec<super::Statment<'a>>,
}
