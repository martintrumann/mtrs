use crate::{expression::block::BlockExpression, typ::Type, Parse};

#[derive(Debug)]
pub struct Function<'a> {
    params: Vec<(&'a str, Type<'a>)>,
    returns: Option<Type<'a>>,
    body: BlockExpression<'a>,
}

impl Parse for Function<'_> {
    fn parse(toks: &mut crate::tokens::Tokens, e: &mut crate::Errors) -> Option<Self> {
        todo!("function")
    }
}
