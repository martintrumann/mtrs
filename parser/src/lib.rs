use lexer::{parse as lex, Span};

#[cfg(test)]
#[macro_use]
mod tests {
    macro_rules! parse {
        ($str:literal, $out:tt :: $var:ident ) => {
            let l = &mut crate::tokens::Tokens::new(lexer::parse($str));
            let s = <$out as Parse>::parse(l, &mut crate::Errors::new());
            assert_eq!(s, Some($out::$var));
            assert!(l.next().is_none());
        };
    }
}

pub mod attribute;
pub mod expression;
pub mod item;
#[path = "macro.rs"]
pub mod mcro;
pub mod path;
pub mod tokens;
#[path = "type.rs"]
pub mod typ;
pub mod visibility;
use tokens::Tokens;

#[derive(Debug, Clone)]
pub struct Error {
    pub msg: MaybeStaticStr,
    pub span: Option<Span>,
    pub loc: (u64, Option<Span>),
}

#[derive(Debug, Clone)]
pub enum MaybeStaticStr {
    Static(&'static str),
    Owned(String),
}

impl From<String> for MaybeStaticStr {
    fn from(v: String) -> Self {
        Self::Owned(v)
    }
}

impl From<&'static str> for MaybeStaticStr {
    fn from(v: &'static str) -> Self {
        Self::Static(v)
    }
}

#[derive(Debug)]
pub struct Errors {
    inner: Vec<Error>,
}

impl Errors {
    pub fn new() -> Self {
        Self { inner: Vec::new() }
    }

    pub fn iter(&self) -> std::slice::Iter<'_, Error> {
        self.inner.iter()
    }

    fn push<T>(&mut self, msg: impl Into<MaybeStaticStr>, t: &Tokens) -> Option<T> {
        self.inner.push(Error {
            msg: msg.into(),
            span: t.span().cloned(),
            loc: t.loc(),
        });
        None
    }

    pub fn is_empty(&self) -> bool {
        self.inner.is_empty()
    }
}

trait Parse: Sized {
    #[allow(unused_variables)]
    fn check(toks: &Tokens) -> bool {
        unimplemented!()
    }

    fn parse(toks: &mut Tokens, e: &mut Errors) -> Option<Self>;
}

pub fn parse(s: &str) -> Result<item::Module<'_>, Errors> {
    let mut tokens = Tokens::new(lex(s));
    let mut e = Errors::new();
    if let Some(module) = item::Module::parse(&mut tokens, &mut e) {
        if e.is_empty() {
            Ok(module)
        } else {
            Err(e)
        }
    } else {
        todo!("handle no input")
    }
}
