#[derive(Debug)]
pub struct Attributes<'a>(Vec<Attribute<'a>>);

impl Attributes<'_> {
    #[deprecated = "No parsing is happening"]
    pub fn empty() -> Self {
        eprintln!("Should not be used");
        Self(Vec::new())
    }
}

#[derive(Debug)]
pub struct Attribute<'a> {
    inner: bool,
    name: &'a str,
    input: Vec<&'a str>,
}
