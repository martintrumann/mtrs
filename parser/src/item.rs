use crate::{attribute::Attributes, mcro::Macro, typ::Type, visibility, Errors, Parse, Tokens};

pub mod function;

#[derive(Debug)]
pub struct AttributedItem<'a> {
    attributes: Attributes<'a>,
    item: OuterItem<'a>,
}

impl Parse for AttributedItem<'_> {
    fn parse(toks: &mut Tokens, e: &mut Errors) -> Option<Self> {
        OuterItem::parse(toks, e).map(|item| Self {
            attributes: Attributes::empty(),
            item,
        })
    }
}

#[derive(Debug)]
pub enum OuterItem<'a> {
    VisItem(VisItem<'a>),
    Macro(Macro),
}

impl Parse for OuterItem<'_> {
    fn parse(toks: &mut Tokens, e: &mut Errors) -> Option<Self> {
        if Macro::check(toks) {
            todo!("macros")
        } else {
            VisItem::parse(toks, e).map(Self::VisItem)
        }
    }
}

#[derive(Debug)]
pub struct VisItem<'a> {
    vis: visibility::Visibility,
    item: Item<'a>,
}

impl Parse for VisItem<'_> {
    fn parse(toks: &mut Tokens, e: &mut Errors) -> Option<Self> {
        Some(Self {
            vis: if visibility::Visibility::check(toks) {
                visibility::Visibility::parse(toks, e)?
            } else {
                Default::default()
            },
            item: Item::parse(toks, e)?,
        })
    }
}

#[derive(Debug)]
pub enum Item<'a> {
    Module(Module<'a>),
    ExternCrate(String),
    UseDeclaration(UseTree<'a>),

    Function(function::Function<'a>),
    Struct(),
    Enum(),
}

impl Parse for Item<'_> {
    fn parse(toks: &mut Tokens, e: &mut Errors) -> Option<Self> {
        match toks.peek()? {
            lexer::Token::KwMod => Module::parse(toks, e).map(Self::Module),
            lexer::Token::KwExtern => {
                todo!("extern block");
            }
            lexer::Token::KwUse => UseTree::parse(toks, e).map(Self::UseDeclaration),
            lexer::Token::KwFn => function::Function::parse(toks, e).map(Self::Function),
            lexer::Token::KwStruct => todo!("structs"),
            lexer::Token::KwEnum => todo!("enum"),
            lexer::Token::KwConst => {
                todo!("check for const items");
            }
            lexer::Token::KwAsync => {
                unimplemented!("async is unsupported");
            }
            _ => e.push("unknown item", toks),
        }
    }
}

#[derive(Debug)]
pub struct Module<'a>(Vec<AttributedItem<'a>>);

impl Parse for Module<'_> {
    fn parse(toks: &mut Tokens, e: &mut Errors) -> Option<Self> {
        let mut out = Vec::new();
        while let Some(i) = AttributedItem::parse(toks, e) {
            out.push(i)
        }
        Some(Self(out))
    }
}

#[derive(Debug)]
pub enum UseTree<'a> {
    Group(&'a str, Vec<UseTree<'a>>),
    Item {
        value: Type<'a>,
        alias: Option<Type<'a>>,
    },
}

impl Parse for UseTree<'_> {
    fn parse(toks: &mut Tokens, e: &mut Errors) -> Option<Self> {
        if !toks.next_is(lexer::Token::KwUse) {
            return e.push("no use token", toks);
        }

        todo!("use")
    }
}
