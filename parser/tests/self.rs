use parser::parse;

use std::fs::read_to_string;

#[test]
fn this() {
    let s = read_to_string("./tests/self.rs").unwrap();

    let out = parse(&s).unwrap();

    println!("{:?}", out);
}

/// and a doc string
#[test]
fn bad() {
    let s = r#"
fn some() {
    just '
}some more text
"#;

    let e = parse(s).unwrap_err();
    let mut e = e.iter();
    let span = e.next().unwrap().span.clone().unwrap();
    assert_eq!(&s[span], "'\n")
}
